package fatiador;

public class ParserArgumentException extends Exception {

    private static final long serialVersionUID = 1L;

    public ParserArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParserArgumentException(String message) {
        super(message);
    }

}
