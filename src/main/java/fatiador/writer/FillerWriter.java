package fatiador.writer;

import fatiador.FlatField;
import fatiador.WriterArgumentException;

public class FillerWriter implements FieldWriter {

	@Override
	public String write(Object value, FlatField flatField) throws WriterArgumentException {
		int size = flatField.getSize();
		char[] spaces = new char[size];
        for (int i = 0; i < size; i++) {
            spaces[i] = ' ';
        }
        return new String(spaces);
	}

}
