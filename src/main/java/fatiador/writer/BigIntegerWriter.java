package fatiador.writer;

import com.google.common.base.Strings;
import fatiador.FlatField;
import fatiador.WriterArgumentException;

import java.math.BigInteger;

public class BigIntegerWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) throws WriterArgumentException {
        int size = flatField.getSize();
        BigInteger intValue = value != null ? (BigInteger) value : BigInteger.valueOf(0);
        String flatNumber = Strings.padStart(intValue.toString(), size, '0');
        if (flatNumber.length() > size) {
            String msg = String.format("Number [%s] too long for field [%s] of size [%d].", intValue.toString(),
                    flatField.getName(), size);
            throw new WriterArgumentException(msg);
        }  
        return flatNumber;
    }

}
