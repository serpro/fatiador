package fatiador.writer;

import org.apache.commons.lang3.BooleanUtils;

import com.google.common.base.Strings;

import fatiador.FlatField;

public class BooleanWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        String booleanString = BooleanUtils.toString((Boolean) value, flatField.getFlatTrue(),
                flatField.getFlatFalse());
        return Strings.padEnd(booleanString, size, ' ');
    }

}
