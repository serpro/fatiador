package fatiador.writer;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateWriter extends AbstractDateTimeWriter<LocalDate> {

    @Override
    public String writeDateTime(LocalDate value, DateTimeFormatter dateFormat) {
        return value.format(dateFormat);
    }

}
