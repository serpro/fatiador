package fatiador.writer;

import fatiador.FlatField;
import fatiador.WriterArgumentException;

public interface FieldWriter {

    String write(Object value, FlatField flatField) throws WriterArgumentException;
    
}
