package fatiador.writer;

import fatiador.FlatType;

public class FieldWriterFactory {

    public FieldWriter getFieldWriter(FlatType flatType) {
        switch (flatType) {
        case ALPHA:
            return new AlphaWriter();
        case NUMERIC:
            return new NumericWriter();
        case INTEGER:
            return new IntegerWriter();
        case BIG_INTEGER:
            return new BigIntegerWriter();
        case DECIMAL:
            return new DecimalWriter();
        case BOOLEAN:
            return new BooleanWriter();
        case DATE:
            return new DateWriter();
        case TIME:
            return new TimeWriter();
        case DATE_TIME:
            return new DateTimeWriter();
        case FILLER:
        	return new FillerWriter();
        case STRUCTURED:
            return new StructuredWriter();
        default:
            throw new IllegalArgumentException("No known Writer for type " + flatType);
        }
    }

}
