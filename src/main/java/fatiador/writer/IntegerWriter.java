package fatiador.writer;

import com.google.common.base.Strings;

import fatiador.FlatField;
import fatiador.WriterArgumentException;

public class IntegerWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) throws WriterArgumentException {
        int size = flatField.getSize();
        Integer intValue = value != null ? (Integer) value : 0;
        String flatNumber = Strings.padStart(intValue.toString(), size, '0');
        if (flatNumber.length() > size) {
            String msg = String.format("Number [%d] too long for field [%s] of size [%d].", intValue,
                    flatField.getName(), size);
            throw new WriterArgumentException(msg);
        }
        return flatNumber;
    }

}
