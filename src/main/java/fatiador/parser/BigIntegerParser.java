package fatiador.parser;

import fatiador.FlatField;
import fatiador.LengthField;
import fatiador.ParserArgumentException;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;

/**
 * BigInteger Type parse values with full numeric content only
 */
public class BigIntegerParser implements FieldParser<BigInteger> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to BigInteger.";

    @Override
    public BigInteger parse(String rawValue, FlatField flatField) throws ParserArgumentException {
    	
    	if (StringUtils.isBlank(rawValue)) {
    		return BigInteger.valueOf(0);
    	}
    	
        try {
            return new BigInteger(rawValue);
        } catch (NumberFormatException e) {
            String fieldName = flatField.getName();
            if (flatField instanceof LengthField) {
                fieldName += " length";
            }
            String msg = String.format(ERROR_MESSAGE, fieldName, rawValue);
            throw new ParserArgumentException(msg, e);
        }
    }

}
