package fatiador.parser;

import org.apache.commons.lang3.StringUtils;

import fatiador.FlatField;
import fatiador.LengthField;
import fatiador.ParserArgumentException;

public class IntegerParser implements FieldParser<Integer> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to Integer.";

    @Override
    public Integer parse(String rawValue, FlatField flatField) throws ParserArgumentException {
    	
    	if (StringUtils.isBlank(rawValue)) {
    		return 0;
    	}
    	
        try {
            return Integer.parseInt(rawValue);
        } catch (NumberFormatException e) {
            String fieldName = flatField.getName();
            if (flatField instanceof LengthField) {
                fieldName += " length";
            }
            String msg = String.format(ERROR_MESSAGE, fieldName, rawValue);
            throw new ParserArgumentException(msg, e);
        }
    }

}
