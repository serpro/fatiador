package fatiador.parser;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

/**
 * 
 * @param <T>
 *            LocalDate, LocalTime or LocalDateTime
 */
public abstract class AbstractDateTimeParser<T> implements FieldParser<T> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to  %s  using the pattern [%s].";

    @Override
    public T parse(String rawValue, FlatField flatField) throws ParserArgumentException {
        T dateTimeValue = null;
        if (notEmpty(rawValue)) {
            // TODO test with invalid pattern
            String pattern = flatField.getDateTimeFormat();
            DateTimeFormatterBuilder dateFormatBuilder = new DateTimeFormatterBuilder()
                    .appendPattern(pattern.replace("S", ""));
            String dateTimeStamp = rawValue;
            if (pattern.endsWith("S")) {
                dateFormatBuilder.appendValue(ChronoField.MILLI_OF_SECOND, 3);
                dateTimeStamp = dateTimeStamp + "00";
            }
            DateTimeFormatter dateFormat = dateFormatBuilder.toFormatter();
            try {
                dateTimeValue = parseDateTime(dateTimeStamp, dateFormat);
            } catch (DateTimeParseException e) {
                String msg = String.format(ERROR_MESSAGE, flatField.getName(), rawValue, flatField.getBeanClass(),
                        pattern);
                throw new ParserArgumentException(msg, e);
            }
        }
        return dateTimeValue;
    }

    public abstract T parseDateTime(String rawValue, DateTimeFormatter dateFormat);

    private boolean notEmpty(String value) {
        return !empty(value);
    }

    // TODO test this... it seems previous implementation in AbstractFlatParser
    // was
    // wrong
    private boolean empty(String value) {
        return value.replace('0', ' ').trim().isEmpty();
    }

}
