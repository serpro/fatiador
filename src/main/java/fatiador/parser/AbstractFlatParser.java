package fatiador.parser;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Splitter;

import fatiador.FlatField;
import fatiador.FlatStructure;
import fatiador.FlatType;
import fatiador.LengthField;
import fatiador.MultipleField;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;

public abstract class AbstractFlatParser<T> {

    private FlatStructure structure;

    private Map<String, Integer> quantityOfChunksOfMultipleFields = new HashMap<>();

    private FieldParserFactory fieldParserFactory = new FieldParserFactory();
    
    public AbstractFlatParser(FlatStructure structure) {
        this.structure = structure;
    }

    public T parse(String flatInput) {
        try {
            return innerParse(flatInput);
        } catch (ParserArgumentException e) {
            String msg = e.getMessage() + "\nFlat string = '" + flatInput + "'; partial debug: " + getBean().toString();
            throw new IllegalArgumentException(msg, e.getCause());
        } catch (Exception e) {
            String msg = "Flat string = '" + flatInput + "'; partial debug: " + getBean().toString();
            throw new IllegalStateException(msg, e);
        }
    }
    
    public abstract void initializeBean();
    
    public abstract T getBean();

    public abstract void setValue(String fieldName, Object value);

    public abstract void addInCollection(String fieldName, Object value) throws ParserArgumentException;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private T innerParse(String flatInput) throws Exception  {
        
        initializeBean();

        int pos = 0;
        for (FlatField flatField : structure.getFields()) {

            String name = flatField.getName();
            int size = flatField.getSize();

            if (flatField instanceof SimpleField) {
            	if (!flatField.isFiller()) {
            		String rawValue = flatInput.substring(pos, pos + size);
            		Object value = parseField(rawValue, flatField);
            		setValue(name, value);
            	}
                pos += size;
            }

            if (flatField instanceof LengthField) {
                String rawValue = flatInput.substring(pos, pos + size);
                int intValue = (Integer) parseField(rawValue, flatField);
                quantityOfChunksOfMultipleFields.put(name, intValue);
                pos += size;
            }

            if (flatField instanceof MultipleField) {
                Integer quantityOfChunks = quantityOfChunksOfMultipleFields.get(name);
                String flatList = flatInput.substring(pos, pos + quantityOfChunks * size);
                if (!flatList.isEmpty()) {
                    Iterable<String> chunks = Splitter.fixedLength(size).split(flatList);
                    for (String chunk : chunks) {
                        Object value = parseField(chunk, flatField);
                        addInCollection(flatField.getName(), value);
                    }
                }
                pos += quantityOfChunks * size;
                if (flatField.listSizeIsFixed()) {
                    int quantityOfEmptyChunks = flatField.getListFixedSize() -  quantityOfChunks;
                    if (quantityOfEmptyChunks > 0) {
                        pos += quantityOfEmptyChunks * size;
                    }
                }
            }

        }

        return getBean();
    }

    private Object parseField(String rawValue, FlatField flatField) throws ParserArgumentException {
        FieldParser<?> fieldParser = fieldParserFactory.getFieldParser(flatField.getFlatType());
        return fieldParser.parse(rawValue, flatField);
    }

}
