package fatiador.parser;

import fatiador.FlatField;
import fatiador.FlatParser;
import fatiador.FlatStructure;
import fatiador.ParserArgumentException;

public class StructuredParser implements FieldParser<Object> {

    @Override
    public Object parse(String rawValue, FlatField flatField) throws ParserArgumentException {
        Class beanClass = flatField.getBeanClass();
        FlatStructure structure = flatField.getStructure();
        FlatParser parser = new FlatParser(structure, beanClass);
        Object subBean = parser.parse(rawValue);
        return subBean;
    }

}
