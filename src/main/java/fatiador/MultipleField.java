package fatiador;

public class MultipleField extends FlatField {

    public MultipleField(String name, int size) {
        super(name, size);
    }

    public MultipleField(String name, int size, FlatType flatType) {
        super(name, size, flatType);
    }

    public MultipleField(String name, int size, FlatType flatType, int listFixedSize) {
        super(name, size, flatType, listFixedSize);
    }

    public MultipleField(String name, FlatStructure structure, Class<?> beanClass) {
        super(name, structure, beanClass);
    }

    public MultipleField(String name, FlatStructure structure, Class<?> beanClass, int listFixedSize) {
        super(name, structure, beanClass, listFixedSize);
    }
}
