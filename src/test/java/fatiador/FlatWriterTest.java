package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Test;

public class FlatWriterTest {

    @Test
    public void shouldWriteSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        Person person = new Person("12345678901", "Leonardo");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleFieldsWithNumericIdNumber() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addNumericField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        Person person = new Person("123", "Leonardo");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "00000000123Leonardo  ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = new Person("12345678901", "Leonardo");
        person.setDogs("rex", "dino", "milu");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  3rex  dino milu ";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteIntegerSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 2);

        Person person = aPerson("12345678901", "Leonardo").withAge(29).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  29";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        Person person = new Person("12345678901", "Leonardo");
        person.setScores(12, 13, 14);

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  03121314";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteDecimalSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo").withWeight(75.99).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  07599";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addIntegerField("age", 2);
        personStructure.addDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo").withAge(29).withWeight(75.99).build();
        person.setScores(12, 13, 14);
        person.setDogs("rex", "dino", "milu");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  031213143rex  dino milu 2907599";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteNullValues() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 2);
        personStructure.addDecimalField("weight", 5, 2);

        Person person = new Person("12345678901", null);
        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901          0000000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteValorDecimalMenorQueTamanhoDecimal() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo  ").withWeight(70.1).build();
        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  07010";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteListasVazias() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = aPerson("12345678901", "Leonardo").build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteListasNulas() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = aPerson("12345678901", "Leonardo").build();
        person.scores = null;
        person.dogNames = null;

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateField() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person person = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  19870607";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateFieldWithNullDate() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        Person person = aPerson("12345678901", "Leonardo").withBirthday(null).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo          ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldNotWriteSimpleDateFieldWithInvalidDateFormat() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        String invalidDateFormat = "YYYYmmDD";
        personStructure.addDateField("birthday", invalidDateFormat);

        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person person = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);

        try {
            writer.write(person);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(invalidDateFormat));
            assertTrue(e.getMessage().contains("birthday"));
        }
    }

    @Test
    public void shouldWriteSimpleTimeField() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addTimeField("birthTime", "HHmmss");

        LocalTime birthTime = LocalTime.of(17, 15, 43);
        Person person = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  171543";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateTimeField() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateTimeField("birthDateTime", "yyyyMMddHHmmss");

        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34);
        Person person = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  19870607160834";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateTimeFieldWithPatternS() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateTimeField("birthDateTime", "yyyyMMddHHmmssS");

        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34, 700 * 1000 * 1000);
        Person person = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  198706071608347";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleTimeFieldWithPatternS() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addTimeField("birthTime", "HHmmssS");

        LocalTime birthTime = LocalTime.of(16, 8, 34, 700 * 1000 * 1000);
        Person person = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  1608347";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteToPrivateFields() {

        FlatStructure bookStructure = new FlatStructure();
        bookStructure.addAlphaField("title", 15);
        bookStructure.addAlphaField("author", 15);

        Book book = new Book("War and Peace", "Leo Tolstoy");

        FlatWriter<Book> writer = new FlatWriter<>(bookStructure, Book.class);
        String flat = writer.write(book);
        String expectedFlat = "War and Peace  Leo Tolstoy    ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalse() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "1", "0");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   0";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalseText() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "true", "false");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   false";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalseTextUppercase() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "TRUE", "FALSE");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   FALSE";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToTrue() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "1", "0");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   1";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToTrueText() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "true", "false");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   true ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToTrueTextUppercase() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "TRUE", "FALSE");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   TRUE ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteMultipleStructuredField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);

        Person person = new Person("12345678901", "Leonardo");
        Dog dog1 = new Dog();
        dog1.name = "Rex";
        dog1.breed = "Husky";
        person.dogs.add(dog1);
        Dog dog2 = new Dog();
        dog2.name = "Totó";
        dog2.breed = "Poodle";
        person.dogs.add(dog2);

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  02Rex  Husky     Totó Poodle    ";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteMultipleStructuredFieldForListFixedSize() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addLengthField("dogs", 2);
        int listFixedSize = 4;
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class, listFixedSize);
        personStructure.addAlphaField("name", 10);

        Person person = new Person("12345678901", "Leonardo");
        Dog dog1 = new Dog();
        dog1.name = "Rex";
        dog1.breed = "Husky";
        person.dogs.add(dog1);
        Dog dog2 = new Dog();
        dog2.name = "Totó";
        dog2.breed = "Poodle";
        person.dogs.add(dog2);

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "1234567890102Rex  Husky     Totó Poodle                                  Leonardo  ";
        assertEquals(expectedFlat, flat);
    }
    
    @Test
    public void shouldWriteTwoMultipleStructuredFields() {

        FlatStructure songStructure = new FlatStructure();
        songStructure.addAlphaField("name", 15);
        songStructure.addAlphaField("artist", 10);

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);
        personStructure.addLengthField("favoriteSongs", 2);
        personStructure.addMultipleStructuredField("favoriteSongs", songStructure, Song.class);

        Person person = new Person("Leonardo");
        person.dogs.add(new Dog("Rex", "Husky"));
        person.dogs.add(new Dog("Totó", "Poodle"));
        person.favoriteSongs.add(new Song("Time", "Pink Floyd"));
        person.favoriteSongs.add(new Song("Nothing to say", "Angra"));

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "Leonardo  02Rex  Husky     Totó Poodle    02Time           Pink FloydNothing to say Angra     ";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteMultipleIntegerFieldForListFixedSize() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        int listFixedSize = 4;
        personStructure.addMultipleIntegerField("scores", 2, listFixedSize);

        Person person = new Person("12345678901", "Leonardo");
        person.scores.add(3);
        person.scores.add(12);

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  0203120000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldTruncLongAlphaValues() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 3);

        Person person = aPerson("12345678901", "Leonardo Alexandre Ferreira Leite").withAge(31).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo A031";

        assertEquals(expectedFlat, flat);
    }
    
    @Test
    public void shouldNotWriteTooLongInteger() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 3);

        Person person = aPerson("12345678901", "Leonardo Alexandre Ferreira Leite").withAge(3100).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        
        try {
            writer.write(person);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), containsString("too long"));
            assertThat(e.getMessage(), containsString("3100"));
            assertThat(e.getMessage(), containsString("age"));
            assertThat(e.getMessage(), containsString("3"));
        }
    }
    
    @Test
    public void shouldWriteNullCollection() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("creditCards", 2);
        personStructure.addMultipleAlphaField("creditCards", 10);

        Person person = new Person("12345678901", "Leonardo");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  00";
        assertEquals(expectedFlat, flat);
    }
    
    @Test
	public void shouldWriteWithFillerField() {
		
		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 10);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);

		Person person = new Person("Bob", 21);

		FlatWriter<Person> writer = new FlatWriter<>(musicianStructure, Person.class);
		String flat = writer.write(person);
		String expectedFlat = "     Bob              21";

		assertEquals(expectedFlat, flat);
	}
    
    @Test
	public void shouldWriteWithFillerFieldInMultipleStructuredField() {

		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 5);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);
		
		FlatStructure bandStructure = new FlatStructure();
		bandStructure.addAlphaField("name", 10);
		bandStructure.addLengthField("people", 2);
		bandStructure.addMultipleStructuredField("people", musicianStructure, Person.class);

		Person musician1 = new Person("Bob", 23);
		Person musician2 = new Person("Joe", 21);
		PeopleGroup band = new PeopleGroup("The Poors");
		band.getPeople().add(musician1);
		band.getPeople().add(musician2);

		FlatWriter<PeopleGroup> writer = new FlatWriter<>(bandStructure, PeopleGroup.class);
		String flat = writer.write(band);
		String expectedFlat = "The Poors 02     Bob         23     Joe         21";

		assertEquals(expectedFlat, flat);
	}
    
    @Test
	public void shouldWriteWithFillerFieldAtTheEnd() {
		
		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 10);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);
		musicianStructure.addFillerField(3);

		Person person = new Person("Bob", 21);

		FlatWriter<Person> writer = new FlatWriter<>(musicianStructure, Person.class);
		String flat = writer.write(person);
		String expectedFlat = "     Bob              21   ";

		assertEquals(expectedFlat, flat);
	}
    
    @Test
    public void shouldWriteWithFillerFieldAtTheEndInMultipleStructuredField() {

		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 5);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);
		musicianStructure.addFillerField(3);
		
		FlatStructure bandStructure = new FlatStructure();
		bandStructure.addAlphaField("name", 10);
		bandStructure.addLengthField("people", 2);
		bandStructure.addMultipleStructuredField("people", musicianStructure, Person.class);

		Person musician1 = new Person("Bob", 23);
		Person musician2 = new Person("Joe", 21);
		PeopleGroup band = new PeopleGroup("The Poors");
		band.getPeople().add(musician1);
		band.getPeople().add(musician2);

		FlatWriter<PeopleGroup> writer = new FlatWriter<>(bandStructure, PeopleGroup.class);
		String flat = writer.write(band);
		String expectedFlat = "The Poors 02     Bob         23        Joe         21   ";

		assertEquals(expectedFlat, flat);
	}

    @Test
    public void shouldWriteWithInheritance() {

        FlatStructure catStructure = new FlatStructure();
        catStructure.addAlphaField("name", 10);
        catStructure.addAlphaField("specie", 10);
        catStructure.addAlphaField("color", 10);

        Cat cat = new Cat("Felix", "cat", "black");
        
        FlatWriter<Cat> writer = new FlatWriter<>(catStructure, Cat.class);
        String flat = writer.write(cat);
        
        String expectedFlat = "Felix     cat       black     ";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldThrowExceptionWhenFlatFieldNameDoesNotExistsInTheBeanClass() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        // Person does not have a fullName attribute:
        personStructure.addAlphaField("fullName", 20);

        Person person = new Person("12345678901", "Paul Di'Anno");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        
        try {
            writer.write(person);
            fail("We expect an error");
        } catch (Exception e) {
            Throwable cause = e;
            boolean found = false;
            while (cause != null) {
                if (cause.getMessage() != null && cause.getMessage().contains("fullName")) {
                    found = true;
                    break;
                }
                cause = cause.getCause();
            }            
            assertTrue(found);
        }        
    }    
}
