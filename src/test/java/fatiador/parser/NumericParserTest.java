package fatiador.parser;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;

public class NumericParserTest {

    NumericParser parser = new NumericParser();
    SimpleField field = new SimpleField("number", 100, FlatType.NUMERIC);

    @Test
    public void shouldParseWithSmallNumber() {
        try {
            parser.parse("123", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWithSmallNumberWithChar() {
        try {
            parser.parse("12C", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("12C"));
        }
    }

    @Test
    public void shouldParseWithFullSequence() {
        try {
            parser.parse("12345678901234567890123456789012345678901234567890", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSpacePadAtTheLeft() {
        try {
            parser.parse("          1234567890123456789012345678901234567890", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSpacePadAtTheRight() {
        try {
            parser.parse("1234567890123456789012345678901234567890          ", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWithAlphaAtTheEnd() {
        try {
            parser.parse("12345678901234567890123456789012345678901234567XYZ", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("1234567XYZ"));
        }
    }

    @Test
    public void shouldThrowExceptionWithAlphaAtTheBegin() {
        try {
            parser.parse("ABC45678901234567890123456789012345678901234567890", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("ABC4567890"));
        }
    }

    @Test
    public void shouldThrowExceptionWithAlphaLowerCaseAtTheMiddle() {
        try {
            parser.parse("12345678901234567890abcdefghij12345678901234567890", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("abcdefghij"));
        }
    }

    @Test
    public void shouldThrowExceptionWithSpecialCharsAtTheMiddle() {
        try {
            parser.parse("12345678901234567890  @  $    12345678901234567890", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("@  $"));
        }
    }

    @Test
    public void shouldThrowExceptionWithSpaceAtTheMiddle() {
        try {
            parser.parse("12345678901234567890          12345678901234567890", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("number"));
            assertTrue(e.getMessage().contains("890          123"));
        }
    }

}
