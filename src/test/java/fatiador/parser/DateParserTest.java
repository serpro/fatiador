package fatiador.parser;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;

public class DateParserTest {

    DateParser parser = new DateParser();
    SimpleField field = new SimpleField("date", "yyyyMMdd", FlatType.DATE);

    @Test
    public void shouldParseWithSpecifiedFormat() {
        try {
            parser.parse("20190314", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithFormatDDMMYYYY() {
        field = new SimpleField("date", "ddMMyyyy", FlatType.DATE);
        try {
            parser.parse("14032019", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWithInvalidDay() {
        try {
            parser.parse("20190355", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("55"));
        }
    }


    @Test
    public void shouldThrowExceptionWithSpacesAtTheBegin() {
        try {
            parser.parse("   20190314", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("   20190314"));
        }
    }

    @Test
    public void shouldThrowExceptionWithAlphaAtTheMiddle() {
        try {
            parser.parse("2019MA14", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("MA"));
        }
    }

    @Test
    public void shouldThrowExceptionWithFormatConflict() {
        try {
            parser.parse("14032019", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("14032019"));
        }
    }

}
