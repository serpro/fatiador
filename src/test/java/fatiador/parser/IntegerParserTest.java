package fatiador.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;

public class IntegerParserTest {

	IntegerParser parser = new IntegerParser();
	SimpleField field = new SimpleField("number", 6, FlatType.INTEGER);

	@Test
	public void shouldParseWithSmallNumber() {
		try {
			parser.parse("123", field);
		} catch (ParserArgumentException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void shouldParseEmptySpaceAsZero() throws Exception {
		int value = parser.parse(" ", field);
		assertEquals(0, value);
	}

	@Test
	public void shouldThrowExceptionWithSmallNumberWithChar() {
		try {
			parser.parse("12C", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("12C"));
		}
	}

	@Test
	public void shouldParseWithFullSequence() {
		try {
			parser.parse("123456", field);
		} catch (ParserArgumentException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void shouldThrowExceptionWithSpacePadAtTheLeft() {
		try {
			parser.parse("   456", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("   456"));
		}
	}

	@Test
	public void shouldThrowExceptionWithSpacePadAtTheRight() {
		try {
			parser.parse("123   ", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("123   "));
		}
	}

	@Test
	public void shouldThrowExceptionWithAlphaLowerCaseAtTheMiddle() {
		try {
			parser.parse("12cd56", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("12cd56"));
		}
	}

	@Test
	public void shouldThrowExceptionWithSpecialCharsAtTheMiddle() {
		try {
			parser.parse("1@3 56", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("1@3 56"));
		}
	}

	@Test
	public void shouldThrowExceptionWithSpaceAtTheMiddle() {
		try {
			parser.parse("123  6", field);
			fail();
		} catch (ParserArgumentException e) {
			assertTrue(e.getMessage().contains("number"));
			assertTrue(e.getMessage().contains("123  6"));
		}
	}

}
