package fatiador;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PeopleGroup {

	private String name;
	private List<Person> people = new ArrayList<>();

	public PeopleGroup() {

	}

	public PeopleGroup(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Person> getPeople() {
		return people;
	}

	public void setPeople(List<Person> people) {
		this.people = people;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, people);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeopleGroup other = (PeopleGroup) obj;
		return Objects.equals(name, other.name) && Objects.equals(people, other.people);
	}

	@Override
	public String toString() {
		return "PeopleGroup [name=" + name + ", people=" + people + "]";
	}

}
