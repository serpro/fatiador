package fatiador;

import static org.junit.Assert.*;

import org.junit.Test;

public class FlatStructureTest {

    @Test
    public void shouldReturnStructureLength() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);
        dogStructure.addIntegerField("age", 2);
        dogStructure.addDecimalField("weight", 4, 2);

        assertEquals(21, dogStructure.length());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotCalculateLengthOfStructureWithMultipleField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);
        dogStructure.addIntegerField("age", 2);
        dogStructure.addDecimalField("weight", 4, 2);
        dogStructure.addLengthField("fleasNames", 2);
        dogStructure.addMultipleAlphaField("fleasNames", 10);

        dogStructure.length();
    }
    
    @Test
    public void shoulCalculateLengthOfStructureWithMultipleFieldWithFixedSize() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);
        dogStructure.addIntegerField("age", 2);
        dogStructure.addDecimalField("weight", 4, 2);
        dogStructure.addLengthField("fleasNames", 2);
        dogStructure.addMultipleAlphaField("fleasNames", 10, 5);

        assertEquals(73, dogStructure.length());
    }
}
