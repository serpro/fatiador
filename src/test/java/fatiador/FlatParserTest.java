package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Test;

public class FlatParserTest {
    
    @Test
    public void shouldParseSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleFieldsWithNumericIdNumber() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addNumericField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        String flatInput = "123        Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("123", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldThrowExceptionWhenNumericFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addNumericField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        String flatInput = "123A       Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("identificationNumber"));
            assertTrue(e.getMessage().contains("123A"));
        }

    }

    @Test
    public void shouldParseMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  3rex  dino milu ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntegerSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 2);

        String flatInput = "12345678901Leonardo  29";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").withAge(29).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03121314";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setScores(12, 13, 14);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseDecimalSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  07599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo", 75.99);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addIntegerField("age", 2);
        personStructure.addDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  031213143rex  dino milu 2907599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo", 29, 75.99);
        expectedPerson.setScores(12, 13, 14);
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseListasVazias() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  000";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldThrowExceptionWhenIntegerFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addIntegerField("age", 2);

        String flatInput = "12345678901Leonardo  2A";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("age"));
            assertTrue(e.getMessage().contains("2A"));
        }
    }

    @Test
    public void shouldThrowExceptionWhenMultipleIntegerFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03122A13";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("scores"));
            assertTrue(e.getMessage().contains("2A"));
        }
    }

    @Test
    public void shouldThrowExceptionWhenLengthFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  A3121314";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("scores"));
            assertTrue(e.getMessage().contains("A3"));
        }
    }

    @Test
    public void shouldThrowExceptionWhenDoubleFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  02A12";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("weight"));
            assertTrue(e.getMessage().contains("02A12"));
        }
    }

    @Test
    public void shouldParseMultipleStructuredField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);

        String flatInput = "12345678901Leonardo  02Rex  Husky     Totó Poodle    ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        Dog expectedDog1 = new Dog();
        expectedDog1.name = "Rex";
        expectedDog1.breed = "Husky";
        expectedPerson.dogs.add(expectedDog1);
        Dog expectedDog2 = new Dog();
        expectedDog2.name = "Totó";
        expectedDog2.breed = "Poodle";
        expectedPerson.dogs.add(expectedDog2);

        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseTwoMultipleStructuredFields() {

        FlatStructure songStructure = new FlatStructure();
        songStructure.addAlphaField("name", 15);
        songStructure.addAlphaField("artist", 10);

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);
        personStructure.addLengthField("favoriteSongs", 2);
        personStructure.addMultipleStructuredField("favoriteSongs", songStructure, Song.class);

        String flatInput = "Leonardo  02Rex  Husky     Totó Poodle    02Time           Pink FloydNothing to say Angra     ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("Leonardo");
        expectedPerson.dogs.add(new Dog("Rex", "Husky"));
        expectedPerson.dogs.add(new Dog("Totó", "Poodle"));
        expectedPerson.favoriteSongs.add(new Song("Time", "Pink Floyd"));
        expectedPerson.favoriteSongs.add(new Song("Nothing to say", "Angra"));

        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseMultipleStructuredFieldForListFixedSize() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addAlphaField("name", 5);
        dogStructure.addAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addLengthField("dogs", 2);
        int listFixedSize = 4;
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class, listFixedSize);
        personStructure.addAlphaField("name", 10);

        String flatInput = "1234567890102Rex  Husky     Totó Poodle                                  Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        Dog dog1 = new Dog();
        dog1.name = "Rex";
        dog1.breed = "Husky";
        expectedPerson.dogs.add(dog1);
        Dog dog2 = new Dog();
        dog2.name = "Totó";
        dog2.breed = "Poodle";
        expectedPerson.dogs.add(dog2);

        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDate() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        String invalidDate = "19871340";
        String flatInput = "12345678901Leonardo  " + invalidDate;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("birthday"));
            assertTrue(e.getMessage().contains(invalidDate));
        }
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDateFormat() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        String invalidDateFormat = "YYYYmmDD";
        personStructure.addDateField("birthday", invalidDateFormat);

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("birthday"));
            assertTrue(e.getMessage().contains(invalidDateFormat));
        }
    }

    @Test
    public void shouldParseSimpleTimeField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addTimeField("birthTime", "HHmmss");

        String flatInput = "12345678901Leonardo  160834";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalTime birthTime = LocalTime.of(16, 8, 34);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseSimpleDateTimeField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateTimeField("birthDateTime", "yyyyMMddHHmmss");

        String flatInput = "12345678901Leonardo  19870607160834";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseSimpleDateTimeWithPatternSField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateTimeField("birthDateTime", "yyyyMMddHHmmssS");

        String flatInput = "12345678901Leonardo  198706071608347";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34, 700 * 1000 * 1000);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseSimpleTimeWithPatternSField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addTimeField("birthTime", "HHmmssS");

        String flatInput = "12345678901Leonardo  1608346";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalTime birthTime = LocalTime.of(16, 8, 34, 600 * 1000 * 1000);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseWritingOnPrivateFields() {

        FlatStructure bookStructure = new FlatStructure();
        bookStructure.addAlphaField("title", 15);
        bookStructure.addAlphaField("author", 15);

        String flatInput = "War and Peace  Leo Tolstoy    ";

        FlatParser<Book> parser = new FlatParser<>(bookStructure, Book.class);
        Book book = parser.parse(flatInput);

        Book expectedBook = new Book("War and Peace", "Leo Tolstoy");
        assertEquals(expectedBook, book);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        String dateBlank = "        ";

        String flatInput = "12345678901Leonardo  " + dateBlank;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateField("birthday", "yyyyMMdd");

        String dateZeros = "00000000";

        String flatInput = "12345678901Leonardo  " + dateZeros;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleTimeFieldToNullWhenFlatTimeIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addTimeField("birthTime", "HHmmss");

        String blankTime = "      ";

        String flatInput = "12345678901Leonardo  " + blankTime;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseBooleanFieldToTrue() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "1";
        Person person = parser.parse(flatInput);

        assertTrue(person.isAdult);
    }
    
    @Test
    public void shouldParseBooleanFieldToFalse() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        
        String flatInput = "0";
        Person person = parser.parse(flatInput);
        
        assertFalse(person.isAdult);
    }
    
    @Test
    public void shouldNotParseInvalidBooleanField() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "7";

        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("isAdult"));
            assertTrue(e.getMessage().contains("7"));
            assertTrue(e.getMessage().contains("0"));
            assertTrue(e.getMessage().contains("1"));
        }

    }

    @Test
    public void shouldShowPartialParseWhenFail() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 15);
        personStructure.addAlphaField("age", 2);

        String invalidAge = "DU";
        String flatInput = "12345678901Homer Simpson  " + invalidAge;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        
        try {
            parser.parse(flatInput);
        } catch (Exception e) {
            assertThat(e.getMessage(), containsString("identificationNumber"));
            assertThat(e.getMessage(), containsString("12345678901"));
            assertThat(e.getMessage(), containsString("name"));
            assertThat(e.getMessage(), containsString("Homer Simpson"));
        }

    }
    

    @Test
    public void shouldParseSimpleDateTimeFieldWithZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addDateTimeField("birthDateTime", "yyyyMMddHHmmssS");

        String flatInput = "12345678901Leonardo  00000000000000000";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseWithInheritance() {

        FlatStructure catStructure = new FlatStructure();
        catStructure.addAlphaField("name", 10);
        catStructure.addAlphaField("specie", 10);
        catStructure.addAlphaField("color", 10);

        String flatInput = "Felix     cat       black     ";

        FlatParser<Cat> parser = new FlatParser<>(catStructure, Cat.class);
        Cat cat = parser.parse(flatInput);

        Cat expectedCat = new Cat("Felix", "cat", "black");
        assertEquals(expectedCat, cat);
    }
    
    @Test
    public void shouldTrimFinalSpaces() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldNotTrimInitialSpaces() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);

        String flatInput = "12345678901  Leonardo";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "  Leonardo");
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseEmptyList() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  0";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
        assertTrue(person.dogs.isEmpty()); // just for clarity
    }
    
    @Test
    public void shouldThrowNiceErrorWhenWritingToNullCollection() {
    
        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        personStructure.addAlphaField("name", 10);
        personStructure.addLengthField("creditCards", 2);
        personStructure.addMultipleAlphaField("creditCards", 10);

        String flatInput = "12345678901Leonardo  011234567890";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        
        try {
            parser.parse(flatInput);
            fail("We expect an error");
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("Collection 'creditCards' is null"));
        }
    }

	@Test
	public void shouldParseWithFillerField() {
		
		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 5);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);

		String flatInput = "01234Bob  Guitar 23"; // ID and instrument will be ignored

		FlatParser<Person> parser = new FlatParser<>(musicianStructure, Person.class);
		Person person = parser.parse(flatInput);

		Person expectedPerson = new Person("Bob", 23);
		assertEquals(expectedPerson, person);
	}
	
	@Test
	public void shouldParseWithFillerFieldInMultipleStructuredField() {
        
		FlatStructure musicianStructure = new FlatStructure();
		musicianStructure.addFillerField(5);
		musicianStructure.addAlphaField("name", 5);
		musicianStructure.addFillerField(7);
		musicianStructure.addIntegerField("age", 2);
		
		FlatStructure bandStructure = new FlatStructure();
		bandStructure.addAlphaField("name", 10);
		bandStructure.addLengthField("people", 2);
		bandStructure.addMultipleStructuredField("people", musicianStructure, Person.class);
		
		String flatInput = "The Poors 0201234Bob  Guitar 2392712Joe  Voice  21"; // ID and instrument will be ignored
		
		FlatParser<PeopleGroup> parser = new FlatParser<>(bandStructure, PeopleGroup.class);
		PeopleGroup band = parser.parse(flatInput);

		Person expectedPerson1 = new Person("Bob", 23);
		Person expectedPerson2 = new Person("Joe", 21);
		PeopleGroup expectedBand = new PeopleGroup("The Poors");
		expectedBand.getPeople().add(expectedPerson1);
		expectedBand.getPeople().add(expectedPerson2);
		
		assertEquals(expectedBand, band);
	}

    @Test
    public void shouldThrowExceptionWhenFlatFieldNameDoesNotExistsInTheBeanClass() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addAlphaField("identificationNumber", 11);
        // Person does not have a fullName attribute:
        personStructure.addAlphaField("fullName", 20);

        String flatInput = "12345678901Paul Di'Anno        ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        
        try {
            parser.parse(flatInput);
            fail("We expect an error");
        } catch (Exception e) {
            Throwable cause = e;
            boolean found = false;
            while (cause != null) {
                if (cause.getMessage() != null && cause.getMessage().contains("fullName")) {
                    found = true;
                    break;
                }
                cause = cause.getCause();
            }            
            assertTrue(found);
        }        
    }
}
