package fatiador.writer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.SimpleField;

public class AlphaWriterTest {

    AlphaWriter writer = new AlphaWriter();

    @Test
    public void truncate_long_alpha_values() {
        SimpleField shortField = new SimpleField("text", 10, FlatType.ALPHA);
        String longText = "A long text to be truncated";
        String flatValue = writer.write(longText, shortField);
        assertEquals("A long tex", flatValue);
    }

}
