package fatiador.writer;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.SimpleField;
import fatiador.WriterArgumentException;

public class BigIntegerWriterTest {

    BigIntegerWriter writer = new BigIntegerWriter();

    @Test
    public void not_write_a_too_long_number() {
        SimpleField shortField = new SimpleField("number", 3, FlatType.BIG_INTEGER);
        BigInteger longNumber = BigInteger.valueOf(12345);
        try {
            writer.write(longNumber, shortField);
            fail();
        } catch (WriterArgumentException e) {
            assertThat(e.getMessage(), containsString("too long"));
            assertThat(e.getMessage(), containsString(longNumber.toString()));
            assertThat(e.getMessage(), containsString("3"));
        }
    }
    
    @Test
    public void write_zero_for_null_value() throws Exception {
        SimpleField field = new SimpleField("number", 3, FlatType.BIG_INTEGER);
        BigInteger nullNumber = null;
        String flatNumber = writer.write(nullNumber, field);
        assertEquals("000", flatNumber);
    }

}
