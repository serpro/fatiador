package fatiador.writer;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import fatiador.SimpleField;
import fatiador.WriterArgumentException;

public class DecimalWriterTest {

    DecimalWriter writer = new DecimalWriter();

    @Test
    public void write_a_long_decimal() throws Exception {
        // long enough so sysout prints it in scientific notation
        SimpleField shortField = new SimpleField("decimalNumber", 10, 2);
        Double numberWithLongDecimalPart = 12345678.9;
//        System.out.println(numberWithLongDecimalPart); // 1.23456789E7
        String flatValue = writer.write(numberWithLongDecimalPart, shortField);
        assertEquals("1234567890", flatValue);
    }
    
    @Test
    public void not_write_a_too_long_number() {
        SimpleField shortField = new SimpleField("decimalNumber", 5, 2);
        Double longNumber = 1234.51;
        try {
            String value = writer.write(longNumber, shortField);
            fail("Write should fail, but produced the value " + value);
        } catch (WriterArgumentException e) {
            String errorMessage = "The exception message does not contain all the relevant information to the user.";
            assertThat(errorMessage, e.getMessage(), containsString("too long"));
            assertThat(errorMessage, e.getMessage(), containsString(longNumber.toString()));
            assertThat(errorMessage, e.getMessage(), containsString("5"));
        }
    }
    
    @Test
    public void truncate_a_too_long_decimal_part() throws Exception {
        SimpleField shortField = new SimpleField("decimalNumber", 5, 2);
        Double numberWithLongDecimalPart = 123.515;
        String flatValue = writer.write(numberWithLongDecimalPart, shortField);
        assertEquals("12351", flatValue);
    }
    
    @Test
    public void write_double_with_no_decimal_part() throws Exception {
        SimpleField shortField = new SimpleField("decimalNumber", 5, 2);
        Double numberWithNoDecimalPart = 123D;
        String flatValue = writer.write(numberWithNoDecimalPart, shortField);
        assertEquals("12300", flatValue);
    }
    
    @Test
    public void write_zero() throws Exception {
        SimpleField shortField = new SimpleField("decimalNumber", 5, 2);
        Double zero = 0D;
        String flatValue = writer.write(zero, shortField);
        assertEquals("00000", flatValue);
    }
    
    @Test
    public void write_zero_for_null_value() throws Exception {
        SimpleField field = new SimpleField("decimalNumber", 5, 2);
        Double nullNumber = null;
        String flatNumber = writer.write(nullNumber, field);
        assertEquals("00000", flatNumber);
    }

}
