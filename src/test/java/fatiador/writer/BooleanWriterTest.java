package fatiador.writer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fatiador.SimpleField;

public class BooleanWriterTest {

    BooleanWriter writer = new BooleanWriter();
    SimpleField booleanField = new SimpleField("fieldName", 1, "1", "0");
    
    @Test
    public void write_true() {
        Boolean value = true;
        String flatValue = writer.write(value, booleanField);
        assertEquals("1", flatValue);
    }

    @Test
    public void write_false() {
        Boolean value = false;
        String flatValue = writer.write(value, booleanField);
        assertEquals("0", flatValue);
    }

}
