package fatiador.writer;

import static org.junit.Assert.*;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.SimpleField;
import fatiador.WriterArgumentException;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class IntegerWriterTest {

    IntegerWriter writer = new IntegerWriter();

    @Test
    public void not_write_a_too_long_inetger() {
        SimpleField shortField = new SimpleField("number", 3, FlatType.INTEGER);
        Integer longNumber = 12345;
        try {
            String value = writer.write(longNumber, shortField);
            fail("Write should fail, but produced the value " + value);
        } catch (WriterArgumentException e) {
            String errorMessage = "The exception message does not contain all the relevant information to the user.";
            assertThat(errorMessage, e.getMessage(), containsString("too long"));
            assertThat(errorMessage, e.getMessage(), containsString(longNumber.toString()));
            assertThat(errorMessage, e.getMessage(), containsString("3"));
        }
    }
    
    @Test
    public void write_zero_for_null_value() throws Exception {
        SimpleField field = new SimpleField("number", 3, FlatType.INTEGER);
        Integer nullNumber = null;
        String flatNumber = writer.write(nullNumber, field);
        assertEquals("000", flatNumber);
    }

}
